
module.exports = function (grunt) {
    grunt.initConfig({
        jshint: {
            files: ['Gruntfile.js'].concat([
                    "app/app.js",
                    "app/directives.js",
                    "app/mainController.js",
                ])
        },
        html2js: {
            options: {
                base: './',
                module: 'moodApp.templates',
                singleModule: true,
                useStrict: true,
                htmlmin: {
                    collapseBooleanAttributes: true,
                    collapseWhitespace: true,
                    removeAttributeQuotes: true,
                    removeComments: false,
                    removeRedundantAttributes: true,
                    removeStyleLinkTypeAttributes: true
                }, rename: function (moduleName) {
                    return '/' + moduleName.replace('./app/', '');
                }
            },
            main: {
                src: ['app/**/*.html'],
                dest: 'dist/moodapp-template-cache.js'
            }
        },
        uglify: {
            app: {
                options: {
                    mangle: false,
                    compress: true
                },
                files: {
                    'dist/vendors.min.js': ['dist/vendors.js'],
                    'dist/app.min.js': ['dist/app.js']
                }
            }
        },
        concat: {
            app: {
                files: [
                    {
                        src: [
                            "app/app.js",
                            "app/directives.js",
                            "app/moodService.js",
                            "app/mainController.js"
                        ], 
                        dest: 'dist/app.js'
                    },
                    {
                        src: [
                            "libs/moment.js",
                            "libs/highcharts.min.js",
                            "libs/angular.min.js",
                            "libs/angular.rangeSlider.js"
                        ], 
                        dest: 'dist/vendors.js'
                    }
                ]
            },
            main: {
                src: [
                    "dist/vendors.min.js", 
                    "dist/moodapp-template-cache.js", 
                    "dist/app.min.js"
                ], 
                dest: './main.js'
            }
        },
        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1,
                keepSpecialComments: 0
            },
            app: {
                files: {
                    './build.min.css': [
                        "css/bootstrap.min.css",
                        "css/style.css",
                        "css/pacifica.min.css",
                        "css/rangeSlider.css"
                    ]
                }
            }
        },
        connect: {
            server: {
                options: {
                    port: 9002,
                    base: './',
                    open: true
                }
            }
        },
        clean: {
            js: ['dist/*.js']
        },
        watch: {
            files: [
                'Gruntfile.js',
                '<%= jshint.files %>',
                'css/*.css'
            ],
            tasks: ['jshint', 'html2js', 'concat:app', 'uglify', 'concat:main', 'cssmin', 'clean'],
            options: {
                livereload: true
            },
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-html2js');

    grunt.registerTask('default', ['jshint', 'html2js', 'concat:app', 'uglify', 'concat:main', 'cssmin', 'clean', 'connect', 'watch']);

    grunt.registerTask('build', ['jshint', 'html2js', 'concat:app', 'uglify', 'concat:main', 'cssmin', 'clean']);

};
