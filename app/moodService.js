
(function () {
    'use strict';

    angular
        .module('moodApp')
        .factory('moodService', moodService);
    moodService.$inject = ['$http'];

    function moodService($http) {

        return {
            moodeValues: moodeValues,
            getFeelings: getFeelings,
            getMoodData: getMoodData,
            getMoodHistory: getMoodHistory,
            addMoodData: addMoodData,
            getWeekData: getWeekData,
            getMoodWeekHistory: getMoodWeekHistory
        };

        function getWeekData() {

            var weekData = [];
            var currentDate = moment();
            var weekStart = currentDate.clone().startOf('week');
            var weekEnd = currentDate.clone().endOf('week');

            for (var i = 0; i < 7; i++) {
                var weekday = {};
                weekday.moment = moment(weekStart).add(i, 'days');
                weekday.day = moment(weekday.moment).format('ddd');
                weekday.date = moment(weekday.moment).format('D');
                weekday.month = moment(weekday.moment).format('M');
                weekday.moods = [];
                weekData.push(weekday);
            }
            return weekData;
        }

        function getFeelings(moodeValues, newval) {
            var temp = '';
            switch (newval) {
                case 0:
                case 1:
                case 2:
                    temp = 'Relaxed';
                    break;
                case 3:
                    temp = 'Calm';
                    break;
                case 4:
                case 5:
                case 6:
                    temp = 'Panicked';
                    break;

                default:
                    break;
            }

            return moodeValues[temp];
        }

        function moodeValues() {
            return {
                "Relaxed": [
                    { key: 'Relaxed', value: 'RELAXED' },
                    { key: 'Loved', value: 'LOVED' },
                    { key: 'Grateful', value: 'Grateful' },
                    { key: 'Excited', value: 'EXCITED' },
                    { key: 'Happy', value: 'HAPPY' },
                    { key: 'Inspired', value: 'INSPIRED' }
                ],
                "Calm": [
                    { key: 'Calm', value: 'CALM' },
                    { key: 'Loved', value: 'LOVED' },
                    { key: 'Grateful', value: 'GRATEFUL' },
                    { key: 'Anxious', value: 'ANXIOUS' },
                    { key: 'Stressed', value: 'STRESSED' },
                    { key: 'Lonely', value: 'LONELY' }],
                "Panicked": [
                    { key: 'Panicked', value: 'PANICKED' },
                    { key: 'Sad', value: 'SAD' },
                    { key: 'Angry', value: 'ANGRY' },
                    { key: 'Anxious', value: 'ANXIOUS' },
                    { key: 'Stressed', value: 'STRESSED' },
                    { key: 'Lonely', value: 'LONELY' }],
            };
        }

        function getMoodData() {
            return $http.get('http://moodtracker.apolloapps.co/api/mood_values');
        }

        function addMoodData(obj) {
            return $http.post("http://moodtracker.apolloapps.co/api/add_mood_history", obj);
        }

        function getMoodHistory(userId) {
            userId = userId || 123456789;
            return $http.get('http://moodtracker.apolloapps.co/api/mood_history/' + userId);
        }

        function getMoodWeekHistory(data) {
            var userId = data.userId || 123456789;
            return $http.get('http://moodtracker.apolloapps.co/api/weekely_history/' + userId + '?minDate=' + data.minDate + '&maxDate=' + data.maxDate);
        }
    }

}());