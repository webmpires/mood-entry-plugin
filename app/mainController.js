
angular.module('moodApp').controller('mainCtrl', ['$scope', '$filter', '$window', 'moodService', function ($scope, $filter, $window, moodService) {

    $scope.moodeValues = moodService.moodeValues();
    $scope.feelings = $scope.moodeValues.Relaxed;
    $scope.moodData = {
        selected: 0,
        habitSubValueIds: [],
    };

    // used when plotting graph in canvas
    // "#60d293", "#5fe9c4", "#5bdddf", "#58caf6", "#ffbe66", "#ff9b73", "#ff6669"
    $scope.moodMappings = {
        "Great": { yOffset: 10, color: "#60d293" },
        "Very Good": { yOffset: 20, color: "#5fe9c4" },
        "Good": { yOffset: 30, color: "#5bdddf" },
        "Okay": { yOffset: 40, color: "#58caf6" },
        "Not Good": { yOffset: 50, color: "#ffbe66" },
        "Bad": { yOffset: 60, color: "#ff9b73" },
        "Awful": { yOffset: 70, color: "#ff6669" }
    };

    $scope.$watch('moodData.selected', function (newval) {
        $scope.feelings = moodService.getFeelings($scope.moodeValues, newval);
    });

    $scope.showView = function (viewType) {
        $scope.selectedView = viewType;
        if (viewType == 'Home') {
            $scope.loadMoodData();
        } else {
            $scope.weekData = angular.copy(moodService.getWeekData());
            $scope.loadHistory();
        }
    };

    $scope.loadMoodData = function () {
        moodService.getMoodData().then(function (res) {
            $scope.habitValues = res.data;
            $scope.habitMoodValues = res.data.habitValues;
        });
    };

    $scope.showView('Home');

    $scope.subValueIsActive = function (val) {
        var idx = $scope.moodData.habitSubValueIds.indexOfAssoc(val, 'valueString');
        return idx !== undefined ? true : false;
    };

    $scope.setSelectedSubValue = function (val) {
        var alreadyseletedIdx = $scope.moodData.habitSubValueIds.indexOfAssoc(val, 'valueString');

        if (alreadyseletedIdx !== undefined) {
            $scope.moodData.habitSubValueIds.splice(alreadyseletedIdx, 1);
            return;
        }
        var seletedMood = $filter('filter')($scope.habitValues.habitSubvalues, { valueString: val });
        if (seletedMood && seletedMood.length)
            $scope.moodData.habitSubValueIds.push(seletedMood[0]);
    };

    $scope.isInProgress = false;

    $scope.updateMood = function () {
        if ($scope.isInProgress) return;
        loginUser = loginUser || {};
        $scope.isInProgress = true;
        var obj = {
            "habitValueId": $scope.habitMoodValues[$scope.moodData.selected].id,
            "experiencedAt": moment().format(),
            "userId": loginUser.userId || '123456789',
            notes: $scope.moodData.notes,
            habitSubValueIds: []
        };

        $scope.moodData.habitSubValueIds.forEach(function (sub) {
            obj.habitSubValueIds.push(sub.id);
        });

        obj.habitSubValueIds = obj.habitSubValueIds.toString();
        moodService.addMoodData(obj).then(function (res) {
            $scope.isInProgress = false;
            $scope.showView('History');
            $scope.moodData = {
                selected: 0,
                habitSubValueIds: [],
            };
        });
    };


    $scope.loadHistory = function () {

        var loginUser = loginUser || {};
        var data = {};
        data.userId = loginUser.userId;
        var week = moment().startOf('week');
        data.minDate = week.format('YYYY-MM-DD');
        data.maxDate = moment(week).add(7, 'days').format('YYYY-MM-DD');

        moodService.getMoodWeekHistory(data).then(function (res) {

            $scope.habitData = res.data.habitData;
            angular.forEach($scope.habitData, function (habitData) {
                habitData.habitSubValueIds = habitData.habitSubValueIds.split(",");
            });

            $scope.avgClass = 0;

            angular.forEach($scope.habitData, function (habitData) {
                habitData.habitValueId = parseInt(habitData.habitValueId);
                $scope.avgClass = $scope.avgClass + habitData.habitValueId;
            });

            $scope.avgClass = Math.ceil(($scope.avgClass / $scope.habitData.length));
            $scope.moodAvgClass = $scope.habitMoodValues[$scope.avgClass].valueString.replace(" ", "");

            $scope.habitSubValues = res.data.habitSubvalues;

            angular.forEach($scope.habitData, function (mood) {
                var temp = [];
                mood.habitSubValueIds.forEach(function (id) {
                    id = parseInt(id);

                    var idx = $scope.habitSubValues.indexOfAssoc(id, 'id');
                    if (idx !== undefined) {
                        var obj = $scope.habitSubValues[idx];
                        $obj.avgClass = $scope.avgClass + obj.id;
                        obj.avgClass = Math.ceil((obj.avgClass / obj.habitData.length));
                        obj.moodAvgClass = $scope.habitMoodValues[obj.avgClass].valueString.replace(" ", "");
                        temp.push(obj);
                    }
                });
                mood.habitSubValueIds = temp;
            });

            console.log("HABIT DATA >>>>>", $scope.habitData);

            // arrange week data from array
            for (var i = 0; i < $scope.weekData.length; i++) {
                for (var j = 0; j < $scope.habitData.length; j++) {
                    var weekDtTmp = moment($scope.weekData[i].moment).format("YYYY-MM-DD");
                    var habitDtTmp = moment($scope.habitData[j].experiencedAt).format("YYYY-MM-DD");
                    if (weekDtTmp === habitDtTmp) {
                        $scope.weekData[i].moods.push($scope.habitData[j]);
                    }
                }
            }

            console.log("FINAL DATA >>> ", $scope.weekData);
            // loadGraph();
            // drawGraph();
            plot();
        });
    };


    console.log("WEEK DATA >>>> ", $scope.weekData);
    Date.prototype.addDays = function (t) {
        var e = new Date(this.valueOf());
        return e.setDate(e.getDate() + t);
    };

    function plot() {

        // drawing dots
        var drawCoordinates = function (ctx, x, y, color) {
            var pointSize = 3; // Change according to the size of the point.

            ctx.fillStyle = color; // Red color

            ctx.beginPath(); //Start path
            ctx.arc(x, y, pointSize, 0, Math.PI * 2, true); // Draw a point using the arc function of the canvas with a point structure.
            ctx.fill(); // Close the path and fill.
        };

        var drawPath = function (ctx, points) {
            ctx.beginPath();
            ctx.moveTo(points[0][0], points[0][1]);
            for (var i = 1; i < points.length; i++) {
                ctx.lineTo(points[i][0], points[i][1]);
            }
            ctx.lineWidth = 0.1;
            ctx.strokeStyle = '#ffffff';
            ctx.stroke();
        };

        var screenWidth = $(document).innerWidth();
      //  $("#container").css({ "width": screenWidth + "px" });
        var canvasTopOffset = $("#container").offset().top + 20;
        var slotWidth = parseInt(screenWidth / 7); // each date slot width
        var canvas = document.getElementById('container');
        var ctx = canvas.getContext('2d');
        var i;

        var points = [];
        for (i = 0; i < $scope.weekData.length; i++) {
            for (var j = 0; j < $scope.weekData[i].moods.length; j++) {
                var x = (i * slotWidth) + parseInt(slotWidth / 2);
                var y = $scope.moodMappings[$scope.weekData[i].moods[j].valueString].yOffset + canvasTopOffset;
                var color = $scope.moodMappings[$scope.weekData[i].moods[j].valueString].color;
                points.push([x, y, color]);
            }
        }
        console.log(" POINTS >>>>> ", points);
        for (i = 0; i < points.length; i++) {
            drawCoordinates(ctx, points[i][0], points[i][1], points[i][2]);
        }

        for (i = 0; i < points.length; i++) {
            drawPath(ctx, points);
        }
    }
}]);
