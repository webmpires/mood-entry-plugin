
var loginUser;

function isJson(str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
}

function gbDidSuccessGetUser(user) {
  if (isJson(user)) {
    loginUser = JSON.parse(user);
  } else { loginUser = user; }
  console.log(loginUser);
}

gbGetUser();

angular.module("moodApp", [
  'ui-rangeSlider',
  'moodApp.templates',
   
]);

angular.module("moodApp").config(function ($httpProvider) {
  $httpProvider.defaults.useXDomain = true;
  delete $httpProvider.defaults.headers.common['X-Requested-With'];
});

Array.prototype.indexOfAssoc = function (value, key) {

  if (!key && key !== 0) {
    throw "Please provide a key to find index of.";
  }

  var i;
  for (i = 0; i < this.length; i++) {

    if (this[i][key] === value) {
      return i;
    }
  }
};